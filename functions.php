<?php 

function theme_styles() {
    wp_enqueue_style( 'vendor_css', get_template_directory_uri() . '/public/css/vendor.css' );
    // //To avoid caching I use time, remove it in production.   
    wp_enqueue_style( 'theme_css', get_template_directory_uri() . '/public/css/app.css?='.time() );

    //For development only! This is where webpack-dev-server would serve the assets from.
    // wp_enqueue_style( 'vendor_css', 'http://localhost:8080/vendor.css' );
    // wp_enqueue_style( 'theme_css', 'http://localhost:8080/app.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

function theme_js() {
    wp_enqueue_script( 'vendor_js', get_template_directory_uri() . '/public/js/vendor.js', '', '', true );    
    //To avoid caching I use time, remove it in production.    
    wp_enqueue_script( 'theme_js', get_template_directory_uri() . '/public/js/app.js?='.time(), array('vendor_js'), '', true );
    wp_localize_script('theme_js', 'wp_api_vuejs', array(
        'nonce' => wp_create_nonce('wp_rest'),
        'success' => __('Post Submitted', 'vuetest'),
        'failure' => __('Post could not be processed.', 'vuetest'),
        'current_user_id' => get_current_user_id(),
        'api_base_url' => home_url(),
        'user_logged_in' => is_user_logged_in()
    ));

    //For development only! This is where webpack-dev-server would serve the assets from.
    // wp_enqueue_script( 'vendor_js', 'http://localhost:8080/vendor.js', '', '', true );
    // wp_enqueue_script( 'theme_js', 'http://localhost:8080/app.js', array('vendor_js'), '', true );
}
add_action( 'wp_enqueue_scripts', 'theme_js' );

function theme_register_meta_api() {
    $meta_fields = array('trivia', 'link');

    foreach ($meta_fields as $field) {
        register_rest_field('post',
            $field,
            array(
                'get_callback' => 'theme_get_meta',
                'update_callback' => 'theme_update_meta',
                'schema' => null
            )
        );
    }
}
add_action('rest_api_init', 'theme_register_meta_api');

function theme_register_custom_meta() {
    // Post Trivia

    // Post Links
}

function theme_get_meta($object, $meta) {
    $post_id = $object['id'];
    return get_post_meta($post_id, $meta);
}

function theme_update_meta($meta, $post, $key) {
    $post_id = $post->ID;
    
    if (!add_post_meta($post_id, $key, $meta, true)) {
        update_post_meta($post_id, $key, $meta);
    }
    
}

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' ); 

 
