import '../scss/app.scss';

import Vue from 'vue';
import VueAlert from '@vuejs-pt/vue-alert';
import welcome from './components/welcome.vue';
import registerUser from './components/registerUser.vue';
import quickPost from './components/quickPost.vue';

//window.axios = require('axios');
window.Vue = Vue;

Vue.component('welcome', welcome);
Vue.component('register-user', registerUser);
Vue.component('quick-post', quickPost);

Vue.use(VueAlert);

const app = new Vue({
    el: '#app',
});